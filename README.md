Small debug tool for Asuswrt and Asuswrt-Merlin users uing Privacy-Filter, Malware-Filter or uBlockr

How to use:
````shell
wget --no-check-certificate https://gitlab.com/swe_toast/debugtool/raw/master/debugtool.sh && sh debugtool.sh && rm debugtool.sh
````
then copy the link produced at the end of the script in the support thread for the script thats not working.

Here is how a typical debug log looks

````
 Router Info
--------------------
Router Model: RT-AC56U
Firmware Version: 380.66-beta1-g7b22cbf
Ipset Version: ipset v6.29, protocol version: 6
IPv6 Status: disabled

 Malware-Filter
--------------------
Malware-Filter Location:
/jffs/scripts/malware-filter
/jffs/malware-filter.list

Installed version: Revision 26

Loaded rules:
malware-filter_ipv4
malware-filter_ipv4_range

services-start presense: present
wan-start presense: present
firewall-start presense: present

Schedule Check:
0 */12 * * * /jffs/scripts/malware-filter #malware-filter#

 Privacy-Filter
--------------------
Privacy-Filter Location:
/jffs/scripts/privacy-filter
/jffs/privacy-filter.list

Installed version: Revision 23

Loaded rules:
privacy-filter_ipv4

services-start presense: present
wan-start presense: present
firewall-start presense: present

Schedule Check:
0 */12 * * * /jffs/scripts/privacy-filter #privacy-filter#
````