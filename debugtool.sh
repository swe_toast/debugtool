#!/bin/sh
# Author: Toast
debug_log=/tmp/debug.log
if [ -f $debug_log ]; then rm $debug_log; fi

detect_environment () {
case $(uname -a) in
 *ASUSWRT-Merlin) path="/jffs/"; OSTYPE='ASUSWRT-Merlin' ;;
      *GNU/Linux) detect_sublinux ;;
           *UBNT) ;;
               *) detect_sublinux ;;
esac;}


detect_sublinux () {
case $(grep -oE 'merlin|padavan|OpenWrt' /proc/version) in
     padavan) path="/opt"; OSTYPE='Padavan' ;;
     OpenWrt) path="/opt"; OSTYPE='OpenWRT' ;;
      merlin) path="/jffs"; OSTYPE='ASUSWRT-Merlin' ;;
esac; }

detect_os_environment () {
if [[ $OSTYPE == 'ASUSWRT-Merlin' ]]; then detected_asuswrt_merlin; fi
if [[ $OSTYPE == 'Padavan' ]]; then detected_padavan; fi }

detected_asuswrt_merlin () {
printf " Router Info\n" >> $debug_log
printf '%s\n' -------------------- >> $debug_log
awk '{ print "Router Model: " $2 "\nFirmware Version: " $3 }' /etc/motd | tail -2 >> $debug_log
     printf "Ipset Version: `ipset -v`" >> $debug_log
     case $(ipset -v | grep -oE "ipset v[0-9]") in
     *v6) printf "\nIPv6 Status: " >> $debug_log
     nvram get ipv6_service >> $debug_log ;;
     esac }

detect_padavan () {
printf " Router Info\n" >> $debug_log
printf '%s\n' -------------------- >> $debug_log
printf "Ipset Version: `ipset -v`" >> $debug_log
}

detect_malware_filter () {
printf "\n Malware-Filter\n" >> $debug_log
printf '%s\n' -------------------- >> $debug_log
printf "Malware-Filter Location:\n" >> $debug_log
find $path | grep -E 'malware-(filter|block)' >> $debug_log
printf "\nInstalled version: " >> $debug_log
find $path | grep -E "malware-(filter|block)" | while read rev; do cat "$rev" | grep -oE "Revision\s[0-9]{1,3}"; done  >> $debug_log
printf "\nNumber of sources: "  >> $debug_log
cat $path/malware-filter.list | wc -l >> $debug_log
printf "\nLoaded rules:\n" >> $debug_log
ipset -L | grep -E "[mM]alware" | awk '{print $2}' >> $debug_log
printf "\nLoaded firewall:\n" >> $debug_log
iptables -L -v -n | grep -E "malware-filter_ipv[46](_range)?" >> $debug_log
if [[ $OSTYPE == 'ASUSWRT-Merlin' ]]; then
printf "\nservices-start presense: $(if [ "$(cat /jffs/scripts/services-start | grep "malware" | wc -l )" -ge 1 ]; then printf "present"; else printf "not found";fi)\n" >> $debug_log
printf "wan-start presense: $(if [ "$(cat /jffs/scripts/wan-start | grep "malware" | wc -l )" -ge 1 ]; then printf "present"; else printf "not found";fi)\n" >> $debug_log
printf "firewall-start presense: $(if [ "$(cat /jffs/scripts/firewall-start | grep "malware" | wc -l )" -ge 1 ]; then printf "present"; else printf "not found";fi)\n" >> $debug_log
printf "\nSchedule Check:\n" >> $debug_log
cru l | grep -E "[mM]alware-(filter|block)" >> $debug_log; fi
}

detect_privacy_filter () {
printf "\n Privacy-Filter\n" >> $debug_log
printf '%s\n' -------------------- >> $debug_log
printf "Privacy-Filter Location:\n" >> $debug_log
find $path | grep privacy-filter >> $debug_log
printf "\nInstalled version: " >> $debug_log
find $path -name "privacy-filter" | while read rev; do cat "$rev" | grep -oE "Revision\s[0-9]{1,3}"; done >> $debug_log
printf "\nNumber of sources: "  >> $debug_log
cat $path/privacy-filter.list | wc -l >> $debug_log
printf "\nLoaded rules:\n" >> $debug_log
ipset -L | grep "privacy" | awk '{print $2}'  >> $debug_log
printf "\nLoaded firewall:\n" >> $debug_log
iptables -L -v -n | grep -E "privacy-filter_ipv[46]" >> $debug_log
if [[ $OSTYPE == 'ASUSWRT-Merlin' ]]; then
printf "\nservices-start presense: $(if [ "$(cat $path/scripts/services-start | grep "privacy" | wc -l )" -ge 1 ]; then printf "present"; else printf "not found";fi)\n" >> $debug_log
printf "wan-start presense: $(if [ "$(cat $path/scripts/wan-start | grep "privacy" | wc -l )" -ge 1 ]; then printf "present"; else printf "not found";fi)\n" >> $debug_log
printf "firewall-start presense: $(if [ "$(cat $path/scripts/firewall-start | grep "privacy" | wc -l )" -ge 1 ]; then printf "present"; else printf "not found";fi)\n" >> $debug_log
printf "\nSchedule Check:\n" >> $debug_log
cru l | grep "privacy-filter" >> $debug_log; fi
if [ -f "$path/scripts/iblocklist-loader.sh" ]; then
printf "\nChecking for incompatible scripts: " >> $debug_log
printf "detected\n" >> $debug_log
fi }

log_reporter () {
read -p "Do you want to review the debug log and send it (y/n)?" choice
case "$choice" in 
  y|Y ) clear
        cat $debug_log
        read -p "Press any key to continue... or press CTRL+C to cancel " -n1 -s
        cat $debug_log | curl -F 'clbin=<-' https://clbin.com
        rm $debug_log;;
  n|N ) echo "Skipped sending and reviewing, program will now cleanup the debug file.."
        rm $debug_log;;
    * ) echo "Selection invalid";;
esac
}

detect_environment
detect_os_environment
detect_malware_filter
detect_privacy_filter
log_reporter
